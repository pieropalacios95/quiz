package modelo;

public class Usuario {

    private final Integer id;
    private String nombre;
    private Float nota;

    public Usuario() {
        this.id = null;
        this.nombre = null;
        this.nota = null;
    }

    public Usuario(Integer id, String nombre, Float nota) {
        this.id = id;
        this.nombre = nombre;
        this.nota = nota;
    }

    public Integer getID() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getNota() {
        return nota;
    }

    public void setNota(Float nota) {
        this.nota = nota;
    }

    @Override
    public String toString() {
        return "Banco de Pregunta {"
                + "\nID = " + id
                + ",\nNombre=" + nombre
                + ",\nNota Correcta=" + nota
                + "\n }";
    }
}
