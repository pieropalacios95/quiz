package modelo;

public class Qui_ask_answer {

    private Integer qui_ask_asw_id;
    private String qui_ask_asw_preguntas;
    private String qui_ask_asw_correcta;
    private String qui_ask_asw_incorrecta_1;
    private String qui_ask_asw_incorrecta_2;
    private String qui_ask_asw_incorrecta_3;

    public Qui_ask_answer() {
        this.qui_ask_asw_id = null;
        this.qui_ask_asw_preguntas = null;
        this.qui_ask_asw_correcta = null;
        this.qui_ask_asw_incorrecta_1 = null;
        this.qui_ask_asw_incorrecta_2 = null;
        this.qui_ask_asw_incorrecta_3 = null;
    }

    public Qui_ask_answer(Integer qui_ask_asw_id, String qui_ask_asw_preguntas, String qui_ask_asw_correcta, String qui_ask_asw_incorrecta_1, String qui_ask_asw_incorrecta_2, String qui_ask_asw_incorrecta_3) {
        this.qui_ask_asw_id = qui_ask_asw_id;
        this.qui_ask_asw_preguntas = qui_ask_asw_preguntas;
        this.qui_ask_asw_correcta = qui_ask_asw_correcta;
        this.qui_ask_asw_incorrecta_1 = qui_ask_asw_incorrecta_1;
        this.qui_ask_asw_incorrecta_2 = qui_ask_asw_incorrecta_2;
        this.qui_ask_asw_incorrecta_3 = qui_ask_asw_incorrecta_3;
    }

    public Integer getQui_ask_asw_id() {
        return qui_ask_asw_id;
    }

    public void setQui_ask_asw_id(Integer qui_ask_asw_id) {
        this.qui_ask_asw_id = qui_ask_asw_id;
    }

    public String getQui_ask_asw_preguntas() {
        return qui_ask_asw_preguntas;
    }

    public void setQui_ask_asw_preguntas(String qui_ask_asw_preguntas) {
        this.qui_ask_asw_preguntas = qui_ask_asw_preguntas;
    }

    public String getQui_ask_asw_correcta() {
        return qui_ask_asw_correcta;
    }

    public void setQui_ask_asw_correcta(String qui_ask_asw_correcta) {
        this.qui_ask_asw_correcta = qui_ask_asw_correcta;
    }

    public String getQui_ask_asw_incorrecta_1() {
        return qui_ask_asw_incorrecta_1;
    }

    public void setQui_ask_asw_incorrecta_1(String qui_ask_asw_incorrecta_1) {
        this.qui_ask_asw_incorrecta_1 = qui_ask_asw_incorrecta_1;
    }

    public String getQui_ask_asw_incorrecta_2() {
        return qui_ask_asw_incorrecta_2;
    }

    public void setQui_ask_asw_incorrecta_2(String qui_ask_asw_incorrecta_2) {
        this.qui_ask_asw_incorrecta_2 = qui_ask_asw_incorrecta_2;
    }

    public String getQui_ask_asw_incorrecta_3() {
        return qui_ask_asw_incorrecta_3;
    }

    public void setQui_ask_asw_incorrecta_3(String qui_ask_asw_incorrecta_3) {
        this.qui_ask_asw_incorrecta_3 = qui_ask_asw_incorrecta_3;
    }

    @Override
    public String toString() {
        return "Banco de Pregunta {"
                + "\nID = " + qui_ask_asw_id
                + ",\nPregunta=" + qui_ask_asw_preguntas
                + ",\nRespuesta Correcta=" + qui_ask_asw_correcta
                + ",\nRespuesta Incorrecta =" + qui_ask_asw_incorrecta_1
                + ",\nRespuesta Incorrecta =" + qui_ask_asw_incorrecta_2
                + ",\nRespuesta Incorrecta =" + qui_ask_asw_incorrecta_3
                + "\n }";
    }
}
