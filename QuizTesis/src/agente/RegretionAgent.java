package agente;

import jade.core.Agent;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Usuario;
import servicio.Conexion;
import servicio.Usuario_servicio;

public class RegretionAgent extends Agent {

    public static double correlacion = 0;
    public static double [] x = null;
    public static double [] y = null;
    
    protected void setup() {

        Usuario_servicio us = new Usuario_servicio();
        Usuario um = new Usuario();
        Conexion cnx = null;

        for (int i = 0; i < 10; i++) {

        }

        int n = 0;
        try {

            List<Usuario> usuarios = us.recuperarTodasById(cnx.obtener());

            n = usuarios.size();

            this.x = new double[n];
            this.y = new double[n];

            for (int i = 0; i < n; i++) {
                x[i] = i+1;
                y[i] = usuarios.get(i).getNota();
            }

            // Utilizo un ciclo 'for' para iterar sobre los elementos del array.
            System.out.print("x[] = ");
            for (int i = 0; i < x.length; i++) {
                // Imprimimos los elementos del array en pantalla.
                System.out.print(" || " + x[i]);
            }
            System.out.println("");
            
            // Utilizo un ciclo 'for' para iterar sobre los elementos del array.
            System.out.print("y[] = ");
            for (int i = 0; i < y.length; i++) {
                // Imprimimos los elementos del array en pantalla.
                System.out.print(" || " + y[i]);
            }
            System.out.println("");

        } catch (SQLException ex) {
            Logger.getLogger(RegretionAgent.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(RegretionAgent.class.getName()).log(Level.SEVERE, null, ex);
        }

        Regresion reg = new Regresion(x, y, n);
        reg.lineal();
        System.out.println("a = " + reg.a);
        System.out.println("b = " + reg.b);

        this.correlacion = reg.correlacion();
        System.out.println("correlacion = " + correlacion);
    }
}

class Regresion {

    private double[] x;
    private double[] y;
    private int n;          //número de datos
    public double a, b;    //pendiente y ordenada en el origen

    public Regresion(double[] x, double[] y, int n) {
        this.x = x;
        this.y = y;
        this.n = n; //número de datos
    }

    public void lineal() {
        double pxy, sx, sy, sx2, sy2;
        pxy = sx = sy = sx2 = sy2 = 0.0;
        for (int i = 0; i < n; i++) {
            sx += x[i];
            sy += y[i];
            sx2 += x[i] * x[i];
            sy2 += y[i] * y[i];
            pxy += x[i] * y[i];
        }
        b = (n * pxy - sx * sy) / (n * sx2 - sx * sx);
        a = (sy - b * sx) / n;
    }

    public double correlacion() {
        //valores medios
        double suma = 0.0;
        for (int i = 0; i < n; i++) {
            suma += x[i];
        }
        double mediaX = suma / n;

        suma = 0.0;
        for (int i = 0; i < n; i++) {
            suma += x[i];
        }
        double mediaY = suma / n;
        //coeficiente de correlación
        double pxy, sx2, sy2;
        pxy = sx2 = sy2 = 0.0;
        for (int i = 0; i < n; i++) {
            pxy += (x[i] - mediaX) * (y[i] - mediaY);
            sx2 += (x[i] - mediaX) * (x[i] - mediaX);
            sy2 += (y[i] - mediaY) * (y[i] - mediaY);
        }
        return pxy / Math.sqrt(sx2 * sy2);
    }
}
