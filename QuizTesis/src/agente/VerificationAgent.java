package agente;

import jade.core.Agent;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import modelo.Usuario;
import quiztesis.FaceDetectionJavaFXX;
import javafx.stage.Stage;
import quiztesis.Page_Inicio;
import quiztesis.Page_Quiz;
import servicio.Conexion;
import servicio.Usuario_servicio;

public class VerificationAgent extends Agent {

    Conexion cnx;

    @Override
    protected void setup() {

        //El precio máximo se recibirá como argumento de entrada.
        try {

            Usuario_servicio us = new Usuario_servicio();
            List<Usuario> usuarios = us.recuperarTodas(cnx.obtener());

            if (usuarios.size() != 0) {
                System.out.println("El test fue realizado por " + usuarios.size() + " estudiantes.");
            } else {
                System.out.println("El test no ha sido realizado por ningun usuario.");

                //Creamos el mensaje CFP(Call For Proposal) cumplimentando sus parámetros
                ACLMessage mensajeCFP = new ACLMessage(ACLMessage.CFP);
                //Protocolo que vamos a utilizar
                mensajeCFP.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
                mensajeCFP.setContent("¿Inicio el sistema?");

                //Indicamos el tiempo que esperaremos por las ofertas.
                mensajeCFP.setReplyByDate(new Date(System.currentTimeMillis() + 15000));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ///////////////////////////////////////////////////////////////////////
        Page_Inicio pi = new Page_Inicio();
        pi.setVisible(true);

        Conexion cnx = null;

        Usuario_servicio us = new Usuario_servicio();

        try {
            List<Usuario> usuarios = us.recuperarTodas(cnx.obtener());
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(VerificationAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
