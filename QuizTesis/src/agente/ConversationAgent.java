/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agente;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 *
 * @author Gorila
 */
public class ConversationAgent extends SimpleBehaviour{

       private boolean finished=false;
       protected void setup(){
               action();
               System.out.println("El agente conversacion esta listo");
        }
    /**
     * 
     * This behaviour is a one Shot.
     * It receives a message tagged with an inform performative, print the content in the console and destroy itself if its equal to 1000
     * @param myagent
     */
       public ConversationAgent(final Agent myagent) {
           super(myagent);
       }

       public void action() {
            //1) receive the message
            final MessageTemplate msgTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM);          
            final ACLMessage msg = this.myAgent.receive(msgTemplate);
            //2) check its caracts
            if (msg != null && msg.getContent().equals("1000")) {       
                System.out.println(this.myAgent.getLocalName()+"<----Result received from "+msg.getSender().getLocalName()+" ,content= "+msg.getContent());
                this.finished=true;

                //3) answer
                final ACLMessage msg2 = new ACLMessage(ACLMessage.INFORM);
                msg2.setSender(this.myAgent.getAID());
                msg2.addReceiver(new AID(msg.getSender().getLocalName(), AID.ISLOCALNAME));     
                msg2.setContent("turn off");
                this.myAgent.send(msg2);

            }else{
                block();// the behaviour goes to sleep until the arrival of a new message in the agent's Inbox.
            }
      }

    public boolean done() { return finished;}
}