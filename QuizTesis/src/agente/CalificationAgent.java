package agente;

import jade.core.Agent;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Usuario;
import quiztesis.Page_Quiz;
import servicio.Conexion;
import servicio.Usuario_servicio;

public class CalificationAgent extends Agent {

    Conexion cnx;

    @Override
    public void setup() {
        System.out.println("El Agente queda pendiente de la calificación del estudiante.");

        //El precio máximo se recibirá como argumento de entrada.
        try {
            Page_Quiz pq = new Page_Quiz();

            Usuario_servicio us = new Usuario_servicio();
            List<Usuario> usuarios = us.recuperarTodasById(cnx.obtener());

            if (!pq.flag) {
                System.out.println("El estudiante no ha rendido la prueba todavía.");
                System.out.println(pq.flag);
                this.doDelete();
            } else {
                System.out.println("La nota del estudiante es: " + usuarios.get(0).getNota() + ".");

                //Creamos el mensaje CFP(Call For Proposal) cumplimentando sus parámetros
                ACLMessage mensajeCFP = new ACLMessage(ACLMessage.CFP);
                //Protocolo que vamos a utilizar
                mensajeCFP.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);

                //Indicamos el tiempo que esperaremos por las ofertas.
                mensajeCFP.setReplyByDate(new Date(System.currentTimeMillis() + 15000));
            }

        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(CalificationAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
