package servicio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Usuario;

public class Usuario_servicio {

    private final String tabla = "usuario";

    public void guardar(Connection conexion, Usuario usuario) throws SQLException {
        try {
            PreparedStatement consulta;
            if (usuario.getID() == null) {
                consulta = conexion.prepareStatement("INSERT INTO " + this.tabla + "(nombre, nota) VALUES(?, ?)");
                consulta.setString(1, usuario.getNombre());
                consulta.setFloat(2, usuario.getNota());
            } else {
                consulta = conexion.prepareStatement("UPDATE " + this.tabla + " SET nombre = ?, nota = ? WHERE id = ?");
                consulta.setString(1, usuario.getNombre());
                consulta.setFloat(2, usuario.getNota());
            }
            consulta.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    public Usuario recuperarPorId(Connection conexion, int id) throws SQLException {
        Usuario usuario = null;
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT nombre, nota FROM " + this.tabla + " WHERE id = ?");
            consulta.setInt(1, id);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                usuario = new Usuario(id, resultado.getString("nombre"), resultado.getFloat("nota"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return usuario;
    }

    public void eliminar(Connection conexion, Usuario usuario) throws SQLException {
        try {
            PreparedStatement consulta = conexion.prepareStatement("DELETE FROM " + this.tabla + " WHERE id = ?");
            consulta.setInt(1, usuario.getID());
            consulta.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    public List<Usuario> recuperarTodas(Connection conexion) throws SQLException {
        List<Usuario> usuarios = new ArrayList<>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT id, nombre, nota FROM " + this.tabla + " ORDER BY nota DESC");
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                usuarios.add(new Usuario(resultado.getInt("id"), resultado.getString("nombre"), resultado.getFloat("nota")));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return usuarios;
    }

    public List<Usuario> recuperarTodasById(Connection conexion) throws SQLException {
        List<Usuario> usuarios = new ArrayList<>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT id, nombre, nota FROM " + this.tabla + " ORDER BY id DESC");
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                usuarios.add(new Usuario(resultado.getInt("id"), resultado.getString("nombre"), resultado.getFloat("nota")));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return usuarios;
    }
}
