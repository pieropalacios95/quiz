package servicio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Qui_ask_answer;

public class Qui_ask_answer_servicio {

    private final String tabla = "qui_ask_answer";

    public void guardar(Connection conexion, Qui_ask_answer qui_ask_answer) throws SQLException {
        try {
            PreparedStatement consulta;
            if (qui_ask_answer.getQui_ask_asw_id() == null) {
                consulta = conexion.prepareStatement("INSERT INTO " + this.tabla + "(qui_ask_asw_preguntas, qui_ask_asw_correcta, qui_ask_asw_incorrecta_1, qui_ask_asw_incorrecta_2, qui_ask_asw_incorrecta_3) VALUES(?, ?, ?, ?, ?)");
                consulta.setString(1, qui_ask_answer.getQui_ask_asw_preguntas());
                consulta.setString(2, qui_ask_answer.getQui_ask_asw_correcta());
                consulta.setString(3, qui_ask_answer.getQui_ask_asw_incorrecta_1());
                consulta.setString(4, qui_ask_answer.getQui_ask_asw_incorrecta_2());
                consulta.setString(5, qui_ask_answer.getQui_ask_asw_incorrecta_3());
            } else {
                consulta = conexion.prepareStatement("UPDATE " + this.tabla + " SET qui_ask_asw_preguntas = ?, qui_ask_asw_correcta = ?, qui_ask_asw_incorrecta_1 = ?, qui_ask_asw_incorrecta_2 = ?, qui_ask_asw_incorrecta_3 = ? WHERE qui_ask_asw_ID = ?");
                consulta.setString(1, qui_ask_answer.getQui_ask_asw_preguntas());
                consulta.setString(2, qui_ask_answer.getQui_ask_asw_correcta());
                consulta.setString(3, qui_ask_answer.getQui_ask_asw_incorrecta_1());
                consulta.setString(4, qui_ask_answer.getQui_ask_asw_incorrecta_2());
                consulta.setString(5, qui_ask_answer.getQui_ask_asw_incorrecta_3());
                consulta.setInt(6, qui_ask_answer.getQui_ask_asw_id());
            }
            consulta.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    public Qui_ask_answer recuperarPorId(Connection conexion, int qui_ask_asw_id) throws SQLException {
        Qui_ask_answer qui_ask_answer = null;
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT qui_ask_asw_preguntas, qui_ask_asw_correcta, qui_ask_asw_incorrecta_1, qui_ask_asw_incorrecta_2, qui_ask_asw_incorrecta_3 FROM "
                    + this.tabla + " WHERE qui_ask_asw_id = ?");
            consulta.setInt(1, qui_ask_asw_id);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                qui_ask_answer = new Qui_ask_answer(qui_ask_asw_id, resultado.getString("qui_ask_asw_preguntas"), resultado.getString("qui_ask_asw_correcta"), resultado.getString("qui_ask_asw_incorrecta_1"), resultado.getString("qui_ask_asw_incorrecta_2"), resultado.getString("qui_ask_asw_incorrecta_3"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return qui_ask_answer;
    }

    public void eliminar(Connection conexion, Qui_ask_answer qui_ask_answer) throws SQLException {
        try {
            PreparedStatement consulta = conexion.prepareStatement("DELETE FROM " + this.tabla + " WHERE qui_ask_asw_id = ?");
            consulta.setInt(1, qui_ask_answer.getQui_ask_asw_id());
            consulta.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    public List<Qui_ask_answer> recuperarTodas(Connection conexion) throws SQLException {
        List<Qui_ask_answer> preguntas = new ArrayList<>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT qui_ask_asw_id, qui_ask_asw_preguntas, qui_ask_asw_correcta, qui_ask_asw_incorrecta_1, qui_ask_asw_incorrecta_2, qui_ask_asw_incorrecta_3 FROM " + this.tabla);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                preguntas.add(new Qui_ask_answer(resultado.getInt("qui_ask_asw_id"), resultado.getString("qui_ask_asw_preguntas"), resultado.getString("qui_ask_asw_correcta"), resultado.getString("qui_ask_asw_incorrecta_1"), resultado.getString("qui_ask_asw_incorrecta_2"), resultado.getString("qui_ask_asw_incorrecta_3")));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return preguntas;
    }
}
