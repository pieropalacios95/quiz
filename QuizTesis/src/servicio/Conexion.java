package servicio;

import java.sql.*;

public class Conexion {

    private static Connection cnx = null;

    public static Connection obtener() throws SQLException, ClassNotFoundException {
        if (cnx == null) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                cnx = DriverManager.getConnection("jdbc:mysql://localhost/quiz", "root", "");
                //System.out.println("Conexion exitosa");
            } catch (SQLException ex) {
                throw new SQLException(ex);
            } catch (ClassNotFoundException ex) {
                throw new ClassCastException(ex.getMessage());
            }
        }
        return cnx;
    }

    public static void cerrar() throws SQLException {
        if (cnx != null) {
            cnx.close();
            System.out.println("Conexion terminada");

        }
    }

    /*
public static void main(String[] args) throws SQLException, ClassNotFoundException {
        obtener();
        Qui_ask_answer_servicio qass = new Qui_ask_answer_servicio();
        qass.guardar(cnx, );
    }
     */
}
