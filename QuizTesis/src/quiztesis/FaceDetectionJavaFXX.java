package quiztesis;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;

import java.io.FileNotFoundException;
import java.io.IOException;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoCapture;

public class FaceDetectionJavaFXX extends Application {

    Mat matrix = null;
    static Boolean rostro = false;

    @Override
    public void start(Stage stage) throws FileNotFoundException, IOException, InterruptedException {
        // Capturando una imagen desde la camara
        FaceDetectionJavaFXX obj = new FaceDetectionJavaFXX();
        WritableImage writableImage = obj.capureFrame();

        // Guardando la imagen
        obj.saveImage();
        // Configurando la vista del imagen
        ImageView imageView = new ImageView(writableImage);

        // Configurando el alto y el ancho de la vista de la imagen
        imageView.setFitHeight(400);
        imageView.setFitWidth(600);

        // Configurando el ratio de la imagen de forma predertemida
        imageView.setPreserveRatio(true);

        // Creando un grupo de objetos en la vista de la imagen
        Group root = new Group(imageView);

        // Creando una escena para visualizar las formas sobre la imagen
        Scene scene = new Scene(root, 500, 400);

        // Editando el titulo de la ventana
        stage.setTitle("Detectando rostro");

        // Añadiendo la escena a la ventana
        stage.setScene(scene);

        // Visualizando los contenidos en las ventanas
        stage.show();

        if (rostro) {
            System.out.println("Existe un rostro");
            Page_Quiz pq = new Page_Quiz();
            pq.setVisible(true);
            
            Thread.sleep(3000);
        } else {
            System.out.println("No existe un rostro\nla aplicación no se iniciará");
        }
    }

    public WritableImage capureFrame() {
        WritableImage writableImage = null;

        // Cargando la OpenCV Core Library
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        // Instanciando la clase VideoCapture (camera:: 0)
        VideoCapture capture = new VideoCapture(0);

        // Leyendo el siguiente video frame desde la camera
        Mat matrix = new Mat();
        capture.read(matrix);

        // Si la camara esta abierta
        if (!capture.isOpened()) {
            System.out.println("Camara no detectada");
            capture.release();
        } else {
            System.out.println("Camara detecteda");
        }

        // Si se detecta un video frame se ejecutan el siguiente codigo
        if (capture.read(matrix)) {
            /////// Detectando rostro en el frame /////

            String file = "C:/Users/Gorila/Documents/NetBeansProjects/quiz/QuizTesis/src/xml/lbpcascade_frontalface.xml";
            CascadeClassifier classifier = new CascadeClassifier(file);

            MatOfRect faceDetections = new MatOfRect();
            classifier.detectMultiScale(matrix, faceDetections);
            System.out.println(String.format("Se han detectado %s rostros",
                    faceDetections.toArray().length));
            capture.release();

            rostro = (faceDetections.toArray().length == 1);

            // Dibujando el cuadrado
            for (Rect rect : faceDetections.toArray()) {
                Imgproc.rectangle(
                        matrix, //Donde dibjuamos el cuadrado
                        new Point(rect.x, rect.y), //Abajo a la izquierda
                        new Point(rect.x + rect.width, rect.y + rect.height), //Arriba a la derecha
                        new Scalar(0, 0, 255) //Colores en RGB
                );
            }
            // Creando BuffredImage desde la matriz
            BufferedImage image = new BufferedImage(matrix.width(), matrix.height(),
                    BufferedImage.TYPE_3BYTE_BGR);

            WritableRaster raster = image.getRaster();
            DataBufferByte dataBuffer = (DataBufferByte) raster.getDataBuffer();
            byte[] data = dataBuffer.getData();
            matrix.get(0, 0, data);

            this.matrix = matrix;
            // Creando el Writable Image
            writableImage = SwingFXUtils.toFXImage(image, null);
        }
        return writableImage;
    }

    public void saveImage() {
        // Configurando la ubicacion de la imagen para guardar
        String file = "C:/PP/facedetected" + Page_Inicio.nombre +".jpg";
        // Instanciadno las clase imagecodecs
        Imgcodecs imageCodecs = new Imgcodecs();
        // Guardando nuevamente
        imageCodecs.imwrite(file, matrix);
    }

    public static void main(String args[]) throws InterruptedException {
        launch(args);

    }

}
